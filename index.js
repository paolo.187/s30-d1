const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Syntax:
	// mongoose.connect('<connection string>', {middlewares})

mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.uhhhr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

	//console.error.bind(console)- print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "Connection Error"))

	// if the connection is successful, this will be the output in our console.
	db.once('open', () => console.log('Connected to the cloud database'))

// Mongoose Schema

const taskSchema = new mongoose.Schema({

	// define the name of our schema- taskSchema
	// new mongoose.Schema method- to make a schema
	// we will be needing the name of the task and its status
	// each field will require a data type

	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});

const Task = mongoose.model('Task', taskSchema);
	// models use schemas and they act as the middleman from the server to our database
	// Model can now be used to run commands for interacting with our database.
	// naming convention- name of model should be capitalized and singular form- 'Task'
	// second parameter is used to specify the schema of the documents that will be stored in the mongoDB collection

// Business Logic
	/*
		1. Add a functionality to check if there are duplicate tasks
			-if the task already exists in the db, we return an error
			-if the task doesn't exists in the db, we add it in the db.
		2. The task data will be coming from the request body.
		3. Create a new Task object with name field property.

	*/

app.post('/tasks', (req, res) => {

	Task.findOne({name: req.body.name}, (err, result) =>{

		if(result != null && result.name === req.body.name){

			return res.send('Duplicate task found.')

		} else {

			let newTask = new Task({

				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {

				if(saveErr){
					return console.error(saveErr)
				} else{

					return res.status(201).send('New Task Created')
				}
			})
		}
	})
})

//retrieving all tasks

app.get('/tasks', (req,res)=>{

	Task.find({}, (err,result)=> {

		if(err){

			return console.log(err);
		}else{

			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Activity

const userSchema = new mongoose.Schema({



	name: String,
	password: String
});

const User = mongoose.model('User', userSchema);

//

app.post('/signup', (req, res) => {

	User.findOne({name: req.body.name, password: req.body.password}, (err, result) =>{

		if(result != null && result.name === req.body.name && result.password === req.body.password){

			return res.send('Duplicate user .')

		} else {

			let newUser = new User({

				name: req.body.name,
				password: req.body.password
				
			})

			newUser.save((saveErr, savedUser) => {

				if(saveErr){
					return console.error(saveErr)
				} else{

					return res.status(201).send('New user with password Created')
				}
			})
		}
	})
})

app.get('/user-find', (req,res)=>{

	User.find({}, (err,result)=> {

		if(err){

			return console.log(err);
		}else{

			return res.status(200).json({
				data: result
			})
		}
	})
})